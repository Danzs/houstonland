﻿using System;
using RE5Q.JsonTypes;
using System.Collections.Generic;
using RE5TWrappers;
using System.IO;
using System.Linq;
using CsvHelper;
using RE5Q.Common;
using RE5Q.FileLoader.SdCountyProperties.Utilities;
using RE5Q.FileLoaders.Core;
using RE5Q.ORM;
using RESTWrappers;

namespace RE5Q.FileLoader.SdCountyProperties
{
    public static class LineCracker
    {
        public static SdCountyPropertiesCSV CrackLine(string Line, int LineNo, string FirstLine, Scad scad)
        {
            // ColumnNames is the first line in file which is usually the names of columns / headers used to identify each column
            // CurrentLineColumns is a list of columns for the current line in the file, this is the value for each column name
            var ColumnNames = CSVUtils.GetLineColumns(FirstLine).Select(c => c.MapHeader()).ToList();
            var CurrentLineColumns = CSVUtils.GetLineColumns(Line).ToList();

            if (ColumnNames.Count != CurrentLineColumns.Count)
            {
                Log.LogMsg($"{Program.SourceName}: LineCracker.CrackLine; Expected same number of row and columns; Line - {LineNo};  Actual colums - {ColumnNames.Count }; Actual rows - {CurrentLineColumns.Count}; Source - {scad.SourceID};  Scad - {scad.ScadID}; [LineCrackingDataError]", LogLevels.Critical);
                return null;
            }

            SdCountyPropertiesCSV sdCountyPropertiesCsv = new SdCountyPropertiesCSV();
            sdCountyPropertiesCsv.DataDate = DateTime.UtcNow;
            sdCountyPropertiesCsv.LineNo = LineNo;

            for (int i = 0; i < ColumnNames.Count; i++)
            {
                string columnName = ColumnNames[i];
                string columnValue = CurrentLineColumns[i];

                sdCountyPropertiesCsv.GetType().GetProperty(columnName)?.SetValue(sdCountyPropertiesCsv, columnValue);
            }

            PostProcessProperty(sdCountyPropertiesCsv);
            return sdCountyPropertiesCsv;
        }

        public static SdCountyPropertiesCSV CrackLine(CsvReader csvReader, Dictionary<string, string> Headers, int LineNo)
        {
            SdCountyPropertiesCSV sdCountyPropertiesCsv = new SdCountyPropertiesCSV
            {
                DataDate = DateTime.UtcNow, LineNo = LineNo
            };
            foreach (var header in Headers.Keys)
            {
                var field = csvReader.GetField(header).Trim();
                sdCountyPropertiesCsv.GetType().GetProperty(Headers[header])?.SetValue(sdCountyPropertiesCsv, field);
            }

            PostProcessProperty(sdCountyPropertiesCsv);
            return sdCountyPropertiesCsv;
        }


        public static List<string> GetFileLines(string externalID)
        {
            string line = string.Empty;
            List<string> lines = new List<string>();

            var CSVStream = RawStoreWrapper.GetStream(externalID);
            using (StreamReader sr = new StreamReader(CSVStream))
                while ((line = sr.ReadLine()) != null)
                    lines.Add(line);

            return lines;
        }

        private static void PostProcessProperty(SdCountyPropertiesCSV sdCountyPropertiesCsv)
        {
            if (LocalWebIndexUtils.NucleusUseCodeMapping.ContainsKey(sdCountyPropertiesCsv.NucleusUseCode))
            {
                sdCountyPropertiesCsv.NucleusUseType =
                    LocalWebIndexUtils.NucleusUseCodeMapping[sdCountyPropertiesCsv.NucleusUseCode].Type;
                sdCountyPropertiesCsv.NucleusUseDescription =
                    LocalWebIndexUtils.NucleusUseCodeMapping[sdCountyPropertiesCsv.NucleusUseCode].Description;

            }
            if (LocalWebIndexUtils.FractionalInterestCodes.ContainsKey(sdCountyPropertiesCsv.FractionalInterestCode))
                sdCountyPropertiesCsv.FractionalInterestDescription = LocalWebIndexUtils.FractionalInterestCodes[sdCountyPropertiesCsv.FractionalInterestCode];

            if (LocalWebIndexUtils.LandUseZoneCodes.ContainsKey(sdCountyPropertiesCsv.LandUseZoneCode))
                sdCountyPropertiesCsv.LandUseZoneDescription = LocalWebIndexUtils.LandUseZoneCodes[sdCountyPropertiesCsv.LandUseZoneCode];


            if (LocalWebIndexUtils.OverlayJurisdictionCodes.ContainsKey(sdCountyPropertiesCsv.OverlayJurisdictionCode))
                sdCountyPropertiesCsv.OverlayJurisdictionDescription = LocalWebIndexUtils.OverlayJurisdictionCodes[sdCountyPropertiesCsv.OverlayJurisdictionCode];


            if (LocalWebIndexUtils.AssessmentLandUseCodes.ContainsKey(sdCountyPropertiesCsv.AssessmentLandUseCode))
                sdCountyPropertiesCsv.AssessmentLandUseDescription = LocalWebIndexUtils.AssessmentLandUseCodes[sdCountyPropertiesCsv.AssessmentLandUseCode];

            if (LocalWebIndexUtils.ParcelSubTypeCodes.ContainsKey(sdCountyPropertiesCsv.ParcelSubTypeCode))
                sdCountyPropertiesCsv.ParcelSubTypeDescription = LocalWebIndexUtils.ParcelSubTypeCodes[sdCountyPropertiesCsv.ParcelSubTypeCode];
            //

            if (!string.IsNullOrEmpty(sdCountyPropertiesCsv.TaxStatusCode))
            {
                switch (sdCountyPropertiesCsv.TaxStatusCode)
                {
                    case "N":
                        sdCountyPropertiesCsv.TaxStatusDescription = "Nontaxable";
                        break;
                    case "T":
                        sdCountyPropertiesCsv.TaxStatusDescription = "Taxable";
                        break;
                }
            }

            if (!string.IsNullOrEmpty(sdCountyPropertiesCsv.QualityClassShape))
            {
                var framingType = sdCountyPropertiesCsv.QualityClassShape.Substring(0, 1);
                if (LocalWebIndexUtils.FramingTypeMapping.ContainsKey(framingType))
                    sdCountyPropertiesCsv.StructureFramingType = LocalWebIndexUtils.FramingTypeMapping[framingType];
            }


                var streetNumber = sdCountyPropertiesCsv.SiteAddressNumber;
            var street = $"{sdCountyPropertiesCsv.SiteAddressStreetName} {sdCountyPropertiesCsv.SiteAddressSuffixOrStreetType}";
            var city = sdCountyPropertiesCsv.SiteAddressCommunityOrPostOfficeName;
            var fullAddress = $"{streetNumber} {street}, {city}, CA";
            sdCountyPropertiesCsv.SPAON = fullAddress;
            sdCountyPropertiesCsv.FullAddress = fullAddress;
            sdCountyPropertiesCsv.PostCode = sdCountyPropertiesCsv.SiteAddressZipCode;
        }

    }
}
