﻿namespace RE5Q.FileLoader.SdCountyProperties
{
    public class NucleusUse
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}