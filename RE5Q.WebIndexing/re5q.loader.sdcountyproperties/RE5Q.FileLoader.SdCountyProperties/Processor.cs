﻿using Dapper;
using OpenQA.Selenium;
using RE5Q.Common;
using RE5Q.JsonTypes;
using RE5Q.ORM;
using RE5Q.WebIndexing.Common;
using RE5Q.WebIndexing.Common.BaseClasses;
using RE5Q.WebIndexing.Common.Logging;
using RE5Q.WebIndexing.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using CsvHelper;
using OpenQA.Selenium.Remote;
using RE5Q.FileLoader.SdCountyProperties.Utilities;
using RE5Q.FileLoader.SdCountyProperties.Utilities.WebIndexing;
using RE5TWrappers;

namespace RE5Q.FileLoader.SdCountyProperties
{
    public class Processor : WebIndexProcessorBase
    {
        Dictionary<string, string> _downloadedFiles = new Dictionary<string, string>();

        protected override void PerformPropertySearch(Scad scad, Source src, string indexURL)
        {
            using (var cITEM = Servers.GetConnection("ITEMDB"))
            using (var cRAW = Servers.GetConnection("RAWDB"))
            {
                if (NavigateToUrl(indexURL))
                {
                    if (IndexResultsPage(cITEM, cRAW, scad) > 0)
                    {
                        ProcessFiles(cITEM, cRAW, scad, indexURL);
                    }
                }
            }
        }

        protected override int IndexResultsPage(IDbConnection cITEM, IDbConnection cRAW, Scad scad)
        {
            string fileName = "Parcels.csv";
            ReadFileCsv();
            int count = 0;
            while (!webDriver.IsElementPresent(By.XPath("//span[@class='metatag-updated']")) && count < 15)
            {
                count++;
                Utils.Wait();
            }
            if (webDriver.IsElementPresent(By.Id("onetrust-accept-btn-handler")) &&
                webDriver.FindElementSafe(By.Id("onetrust-accept-btn-handler")) is IWebElement AcceptAllCookiesButton)
            {
                AcceptAllCookiesButton.Click();
            }
            var div = webDriver.FindElementSafe(By.XPath("//span[@class='metatag-updated']"));
            var updated = div.Text;
            webDriver.FindElementSafe(By.XPath("//button[@class='btn dropdown-toggle btn-default hub-download']")).Click();
            var csvDownloadLink = ((RemoteWebDriver)webDriver).FindElementByLinkText("Spreadsheet");

            string fileURL = csvDownloadLink.GetAttribute("href");
            string fileDescription = $"SdCountyProperties Updated {updated}";

            webDriver.GetAndScrollToElement(csvDownloadLink);
            csvDownloadLink.Click();

            bool fileDownloading = true;
            do
            {
                Utils.Wait(10000);

                List<string> directoryFiles = Directory.GetFiles(_p.DownloadDirectoryPath).ToList();
                if (directoryFiles.Count == 1
                    && !directoryFiles.Single().EndsWith("tmp", StringComparison.InvariantCultureIgnoreCase)
                    && !directoryFiles.Single().EndsWith("crdownload", StringComparison.InvariantCultureIgnoreCase))
                {
                    fileDownloading = false;
                }
            }
            while (fileDownloading);
            if (Directory.GetFiles(_p.DownloadDirectoryPath, fileName).SingleOrDefault() is string filePath)
            {
                //store the file (RAW)
                if (StoreFile(cRAW, scad, fileDescription, fileURL, filePath, "EN") is PropertyFile storedFile)
                {
                    string externalID = storedFile.ExternalID;

                    using (var cWIDB = Servers.GetConnection("WEBINDEXINGDB"))
                    {
                        string getAssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToLower();

                        if (externalID.ToUpper().Contains("DUPLICATE"))
                        {
                            Logger.LogWarning("DuplicateFileFound", message: $"Duplicate file found on Raw. File {filePath} - Retrying in 24 hours.");

                            cWIDB.Query($"UPDATE webindexerstate SET WebIndexerDataStaleIn = '24:00:00' " +
                                        $"WHERE TargetWebIndexer = '{getAssemblyName}'", commandTimeout: 60);
                        }
                        else
                        {
                            _downloadedFiles.Add(filePath, externalID);
                            Logger.LogInfo("NewFileFound", message: $"File added for processing - {filePath}");

                            string hourDiff = cWIDB.QuerySingleOrDefault<string>($"SELECT CAST(TIMESTAMPDIFF(HOUR, RunStartedAt, DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 1 MONTH), '%Y-%m-01')) + 8 AS int) " +
                                        $"FROM webindexerstate WHERE TargetWebIndexer = '{getAssemblyName}'", commandTimeout: 60);

                            cWIDB.Query($"UPDATE webindexerstate SET WebIndexerDataStaleIn = '{hourDiff}:00:00' " +
                                        $"WHERE TargetWebIndexer = '{getAssemblyName}'", commandTimeout: 60);
                        }
                    }
                }

            }

            return _downloadedFiles.Count;
        }

        private void ProcessFiles(IDbConnection cITEM, IDbConnection cRAW, Scad scad, string sourceURL)
        {
            foreach (KeyValuePair<string, string> downloadedFile in _downloadedFiles)
            {
                string file = downloadedFile.Key;
                Logger.LogInfo("StartingFileCracking", message: $"Started file cracking on file - {file}");

                string ExternalID = downloadedFile.Value;

                Raw rawFile = Raw.GetByExternalID(cRAW, ExternalID);
                rawFile.SourceUrl = sourceURL;
                rawFile.Save(cRAW);
                int LineNo = 0;

                using (Stream CSVStream = RawStoreWrapper.GetStream(ExternalID))
                {
                    var streamReader = new StreamReader(CSVStream);
                    var csvReader = new CsvReader(streamReader, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    csvReader.Read();
                    csvReader.ReadHeader();
                    var headers = csvReader.Context.HeaderRecord.ToDictionary(h => h, h => h.MapHeader());
                    while (csvReader.Read())
                    {
                        if (LineCracker.CrackLine(csvReader, headers, LineNo++) is SdCountyPropertiesCSV ccData &&
                            ValidateAndNormalise(ref ccData, LineNo))
                        {
                            SaveNewItem(cITEM, scad, ccData, LineNo, ExternalID, rawFile);
                        }
                        else
                        {
                            Logger.LogError("BadDataFormat",
                                message:
                                $"ExternalID {rawFile.ExternalID} line {LineNo} was of a bad format. CrackLine FAILED : no item could be created");
                        }

                        Console.Write($"\rLoading SdCountyProperties Data: {LineNo}                       ");
                    }
                }


                /* List<string> Lines = LineCracker.GetFileLines(ExternalID);
                 int LinesCount = Lines.Count;
                 string FirstLine = Lines.First();
                 for (LineNo = 1; LineNo < LinesCount; LineNo++)
                 {
                     string Ln = Lines[LineNo];

                     if (LineCracker.CrackLine(Ln, LineNo, FirstLine, scad) is SdCountyPropertiesCSV ccData)
                     {
                         ValidateAndNormalise(ref ccData, LineNo);
                         Item item = new Item()
                         {
                             ScadID = scad.ScadID,
                             SourceID = scad.SourceID,
                             FullAddress = ccData.FullAddress,
                             PostCode = ccData.PostCode,
                             SPAON = ccData.SPAON,
                             SourceExternalID = ExternalID,
                             RawID = rawFile.RawID,
                             ItemData = ccData.ToJson(),
                             LayerType = "RAW"
                         };

                         item.Save(cITEM);
                         Logger.LogInfo("ItemSaved",
                             message: $"ExternalID {rawFile.ExternalID} line {LineNo} Saved Item {item.ItemID}");
                     }
                     else
                     {
                         Logger.LogError("BadDataFormat",
                             message:
                             $"ExternalID {rawFile.ExternalID} line {LineNo} was of a bad format. CrackLine FAILED : no item could be created");
                     }

                     Console.Write($"\rLoading SdCountyProperties Data: {LineNo}/{LinesCount}                       ");
                 }*/

                Logger.LogInfo("CrackingProcessComplete", message: $"Source {Program.SourceName} Scad {rawFile.ScadID}: {LineNo} lines processed");

                File.Delete(file);
            }

            Logger.LogInfo("AllFilesProcessed", message: $"Source {Program.SourceName}; ScadID = {scad.ScadID};");
        }

        private static void SaveNewItem(IDbConnection cITEM, Scad scad, SdCountyPropertiesCSV ccData, int LineNo, string ExternalID, Raw rawFile)
        {
            Item item = new Item()
            {
                ScadID = scad.ScadID,
                SourceID = scad.SourceID,
                FullAddress = ccData.FullAddress,
                PostCode = ccData.PostCode,
                SPAON = ccData.SPAON,
                SourceExternalID = ExternalID,
                RawID = rawFile.RawID,
                ItemData = ccData.ToJson(),
                LayerType = "RAW"
            };

            item.Save(cITEM);
            Logger.LogInfo("ItemSaved",
                message: $"ExternalID {rawFile.ExternalID} line {LineNo} Saved Item {item.ItemID}");
        }

        private static bool ValidateAndNormalise(ref SdCountyPropertiesCSV ccData, int LineNo)
        {
            bool result = true;
            // Get PostCode
            //ccData.PostCode = AddressValidation.ValidatePostcode(ccData.IndexedPostcode, ccData.Address);

            //ccData.FullAddress = ccData.Address;
            try
            {
                ccData.FullAddress = AddressValidation.NormaliseAddress(ccData.FullAddress, ccData.PostCode);
                ccData.SPAON = AddressValidation.ValidateSPAON(ccData.FullAddress);
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        private void ReadFileCsv()
        {
            var csvPath = @"C:\Minsk\FallenInWar\parcel_address\Parcels.csv";
            using (Stream CSVStream = CreateFileStream(csvPath))
            {
                var streamReader = new StreamReader(CSVStream);
                var csvReader = new CsvReader(streamReader,
                    System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                csvReader.Read();
                csvReader.ReadHeader();
                int LineNo = 0;

                var headers = csvReader.Context.HeaderRecord.ToDictionary(h => h, h => h.MapHeader());
                while (csvReader.Read())
                {
                    try
                    {
                        if (LineCracker.CrackLine(csvReader, headers, LineNo++) is SdCountyPropertiesCSV ccData &&
                            ValidateAndNormalise(ref ccData, LineNo))
                        {
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }
        private FileStream CreateFileStream(string csvPath)
        {
            try
            {
                return new FileStream(csvPath, FileMode.Open);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        /*private void HidePageOverLays()
        {
            webDriver.ScrollToBottom(); //otherwise floating header bar may not exist                     
            webDriver.ScrollToTop();
        }*/
    }
}
