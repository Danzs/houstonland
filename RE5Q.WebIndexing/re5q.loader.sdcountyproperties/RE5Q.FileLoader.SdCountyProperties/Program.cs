﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RE5Q.FileLoader.SdCountyProperties.Utilities;
using RE5Q.WebIndexing.Common.BaseClasses;

namespace RE5Q.FileLoader.SdCountyProperties
{
    public class Program : WebIndexProgramBase
    {
        private Program()
        {
            SourceName = "DeveloperDaviner"; 
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.RunAsync(args).Wait();
        }

        public override void Run(string[] args)
        {
            ProcessorInstance = new Processor();
            ProcessorKill = ProcessorInstance.Kill;

            if (SetupWebIndexer(args, out _src, out _scad, true))
            {
                ProcessorInstance.StartWebIndexing(_scad, _src, LocalWebIndexUtils.BaseURL, this);

                //if (CommandLineURLs != null && CommandLineURLs.Length > 0) break; //DEBUGGING

                FinishWebIndexing();
            }
        }
    }
}
