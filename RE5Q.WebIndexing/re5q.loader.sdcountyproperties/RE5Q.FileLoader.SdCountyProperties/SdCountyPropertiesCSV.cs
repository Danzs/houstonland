﻿using RE5Q.JsonTypes;
namespace RE5Q.FileLoader.SdCountyProperties
{
    public class SdCountyPropertiesCSV : BaseCSV
    {
        public string OBJECTID { get; set; }
        public string APN { get; set; }
        public string APNFirst8Digits { get; set; }
        public string InternalParcelPolygonID { get; set; }
        public string FractionalInterestCode { get; set; }
        public string FractionalInterestDescription { get; set; }

        public string SitusJurisdiction { get; set; }
        public string SiteAddressStreetName { get; set; }
        public string SiteAddressSuffixOrStreetType { get; set; }
        public string RoadPostDirection { get; set; }
        public string RoadPrefixDirection { get; set; }
        public string SiteAddressNumber { get; set; }
        public string SiteAddressFraction { get; set; }
        public string SiteBuilding { get; set; }
        public string SuiteUnitOrBusinessLocationWithinBuilding { get; set; }
        public string ParcelLegalDescription { get; set; }
        public string AssessedLandValue { get; set; }
        public string AssessedImprovementValue { get; set; }
        public string TotalAssessedValue { get; set; }
        public string DocumentTypeCreatedTheParcel { get; set; }
        public string DocumentNumberCreatedTheParcel { get; set; }
        public string DocumentRecordingDateCreatedTheParcel { get; set; }
        public string ParcelTaxableAcreage { get; set; }
        public string TaxStatusCode { get; set; }
        public string TaxStatusDescription { get; set; }

        public string OwnerOccupiedIndicator { get; set; }
        public string TaxRateAreaNumber { get; set; }
        public string AssessorInfoZone { get; set; }
        public string AssessmentLandUseCode { get; set; }
        public string AssessmentLandUseDescription { get; set; }

        public string NumberOfDwellingUnits { get; set; }
        public string SubdivisionMapNumber { get; set; }
        public string SubdivisionName { get; set; }
        public string LandUseZoneCode { get; set; }
        public string LandUseZoneDescription { get; set; }

        public string NucleusUseCode { get; set; }
        public string NucleusUseType { get; set; }
        public string NucleusUseDescription { get; set; }

        public string SiteAddressCommunityOrPostOfficeName { get; set; }
        public string EffectiveYearTheStructureWasBuilt { get; set; }
        public string TotalLivingAreaSqFt { get; set; }
        public string NumberOfBedrooms { get; set; }
        public string NumberOfBaths { get; set; }
        public string AmountOfAreaAdded { get; set; }
        public string GarageConversion { get; set; }
        public string NumberOfGarageStalls { get; set; }
        public string NumberOfCarportStalls { get; set; }
        public string ParcelContainsPool { get; set; }
        public string DoesThePropertyHaveAView { get; set; }
        public string LevelPadUsableAreaSqFt { get; set; }
        public string QualityClassShape { get; set; }
        public string StructureFramingType { get; set; }
        public string NucleusSitusFromNbr { get; set; }
        public string NucleusSitusThruNbr { get; set; }
        public string NucleusSitusThruFractn { get; set; }
        public string SiteAddressZipCode { get; set; }
        public string XCoordinateOfApproximateParcelCentroid { get; set; }
        public string YCoordinateOfApproximateParcelCentroid { get; set; }
        public string OverlayJurisdictionCode { get; set; }
        public string OverlayJurisdictionDescription { get; set; }

        public string ParcelSubTypeCode { get; set; }
        public string ParcelSubTypeDescription { get; set; }

        public string MoreThanOneAPN { get; set; }
        public string ShapeLength { get; set; }
        public string ShapeArea { get; set; }

    }
}