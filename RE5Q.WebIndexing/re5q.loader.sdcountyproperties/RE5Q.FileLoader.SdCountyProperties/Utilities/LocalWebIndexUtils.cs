﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace RE5Q.FileLoader.SdCountyProperties.Utilities
{
    /// <summary>
    /// For general project utilities and extensions
    /// </summary>
    internal static class LocalWebIndexUtils
    {
        public static string BaseURL = "https://hub.arcgis.com/datasets/c2a97c7a8f4b42d280f8113765d9b635_0";

        public static string[] URLS()
        {
            string[] URLS =
            {
                "https://hub.arcgis.com/datasets/c2a97c7a8f4b42d280f8113765d9b635_0",
            };

            return URLS;
        }

        public static Dictionary<string, string> ColumnHeaderMapping = new Dictionary<string, string>
        {
            {"APN_8", "APNFirst8Digits"},
            {"PARCELID", "InternalParcelPolygonID"},
            {"FRACTINT", "FractionalInterestCode"},
            {"SITUS_JURIS", "SitusJurisdiction"},
            {"SITUS_STREET", "SiteAddressStreetName"},
            {"SITUS_SUFFIX", "SiteAddressSuffixOrStreetType"},
            {"SITUS_POST_DIR", "RoadPostDirection"},
            {"SITUS_PRE_DIR", "RoadPrefixDirection"},
            {"SITUS_ADDRESS", "SiteAddressNumber"},
            {"SITUS_FRACTION", "SiteAddressFraction"},
            {"SITUS_BUILDING", "SiteBuilding"},
            {"SITUS_SUITE", "SuiteUnitOrBusinessLocationWithinBuilding"},
            {"LEGLDESC", "ParcelLegalDescription"},
            {"ASR_LAND", "AssessedLandValue"},
            {"ASR_IMPR", "AssessedImprovementValue"},
            {"ASR_TOTAL", "TotalAssessedValue"},
            {"DOCTYPE", "DocumentTypeCreatedTheParcel"},
            {"DOCNMBR", "DocumentNumberCreatedTheParcel"},
            {"DOCDATE", "DocumentRecordingDateCreatedTheParcel"},
            {"ACREAGE", "ParcelTaxableAcreage"},
            {"TAXSTAT", "TaxStatusCode"},
            {"OWNEROCC", "OwnerOccupiedIndicator"},
            {"TRANUM", "TaxRateAreaNumber"},
            {"ASR_ZONE", "AssessorInfoZone"},
            {"ASR_LANDUSE", "AssessmentLandUseCode"},
            {"UNITQTY", "NumberOfDwellingUnits"},
            {"SUBMAP", "SubdivisionMapNumber"},
            {"SUBNAME", "SubdivisionName"},
            {"NUCLEUS_ZONE_CD", "LandUseZoneCode"},
            {"NUCLEUS_USE_CD", "NucleusUseCode"},
            {"SITUS_COMMUNITY", "SiteAddressCommunityOrPostOfficeName"},
            {"YEAR_EFFECTIVE", "EffectiveYearTheStructureWasBuilt -"},
            {"TOTAL_LVG_AREA", "TotalLivingAreaSqFt"},
            {"BEDROOMS", "NumberOfBedrooms"},
            {"BATHS", "NumberOfBaths"},
            {"ADDITION_AREA", "AmountOfAreaAdded"},
            {"GARAGE_CONVERSION", "GarageConversion"},
            {"GARAGE_STALLS", "NumberOfGarageStalls"},
            {"CARPORT_STALLS", "NumberOfCarportStalls"},
            {"POOL", "ParcelContainsPool"},
            {"PAR_VIEW", "DoesThePropertyHaveAView"},
            {"USABLE_SQ_FEET", "LevelPadUsableAreaSqFt"},
            {"QUAL_CLASS_SHAPE", "QualityClassShape"},
            {"NUCLEUS_SITUS_FROM_NBR", "NucleusSitusFromNbr"},
            {"NUCLEUS_SITUS_THRU_NBR", "NucleusSitusThruNbr"},
            {"NUCLEUS_SITUS_THRU_FRACTN", "NucleusSitusThruFractn"},
            {"SITUS_ZIP", "SiteAddressZipCode"},
            {"x_coord", "XCoordinateOfApproximateParcelCentroid"},
            {"y_coord", "YCoordinateOfApproximateParcelCentroid"},
            {"overlay_juris", "OverlayJurisdictionCode"},
            {"sub_type", "ParcelSubTypeCode"},
            {"multi", "MoreThanOneAPN"},
            {"SHAPE_Length", "ShapeLength"},
            {"SHAPE_Area", "ShapeArea"}
        };

        public static Dictionary<string, NucleusUse> NucleusUseCodeMapping = GetNucleusUseCodeMapping();

        public static Dictionary<string, string> FractionalInterestCodes = new Dictionary<string, string> {
            { "1", "single interest"},
            { "2", "multiple interests, equal shares"},
            { "3", "multiple interests, unequal shares"},
            { "4", "multiple interests, shares unknown"},
            { "5", "all as joint tenants"}
        };

        public static Dictionary<string, string> LandUseZoneCodes = new Dictionary<string, string> {
            {"00", "UNZONED"},
            {"10", "SINGLE FAMILY RESIDENTIAL"},
            {"20", "MINOR MULTIPLE"},
            {"21", "RESTRICTED MINOR MULTIPLE"},
            {"30", "RESTRICTED MULTIPLE"},
            {"31", "RESTRICTED RESTRICTED MULTIPLE"},
            {"40", "MULTIPLE RESIDENTIAL"},
            {"41", "RESTRICTED MULTIPLE RESIDENTIAL"},
            {"50", "RESTRICTED COMMERCIAL"},
            {"60", "COMMERCIAL"},
            {"70", "INDUSTRIAL"},
            {"80", "AGRICULTURAL"},
            {"90", "SPECIAL AND/OR MISC."}
        };

        public static Dictionary<string, string> OverlayJurisdictionCodes = new Dictionary<string, string> {
            {"CB", "Carlsbad"},
            {"CN", "Unincorporated"},
            {"CO", "Coronado"},
            {"CV", "Chula Vista"},
            {"DM", "Del Mar"},
            {"EC", "El Cajon"},
            {"EN", "Encinitas"},
            {"ES", "Escondido"},
            {"IB", "Imperial Beach"},
            {"LG", "Lemon Grove"},
            {"LM", "La Mesa"},
            {"NC", "National City"},
            {"OC", "Oceanside"},
            {"PW", "Poway"},
            {"SD", "San Diego"},
            {"SM", "San Marcos"},
            {"SO", "Solana Beach"},
            {"ST", "Santee"},
            {"VS", "Vista"}
        };

        public static Dictionary<string, string> AssessmentLandUseCodes = new Dictionary<string, string> {
            {"06", "INFORMATION PARCEL- GENERIC"},
            {"07", "TIME SHARE GENERIC"},
            {"09", "MANUFACTURED HOME IN PARK - NOT SPECIFIED"},
            {"10", "VACANT RESIDENTIAL-GENERIC"},
            {"11", "SINGLE FAMILY RESIDENTIAL-GENERIC"},
            {"12", "DUPLEX-GENERIC"},
            {"13", "MULTIPLE 2 TO 4 UNITS-GENERIC"},
            {"14", "MULTIPLE 5 TO 15 UNITS-GENERIC"},
            {"15", "MULTIPLE 16 TO 60 UNITS-GENERIC"},
            {"16", "MULTIPLE 61 UNITS AND UP-GENERIC"},
            {"17", "CONDOMINIUMS AND OTHER RESIDENTAL CLASSIFICATIONS"},
            {"18", "CO-OP GENERIC"},
            {"19", "SPECIAL- SLIVER, SMALL PARCEL"},
            {"20", "VACANT LAND COMMERCIAL"},
            {"21", "GENERIC COMMERCIAL OFFICE/RETAIL 1-3 STORIES"},
            {"22", "GENERIC-4 AND MORE STORY OFFICE BUILDING"},
            {"23", "REGIONAL SHOPPING CENTER"},
            {"24", "COMMUNITY SHOPPING CENTER"},
            {"25", "NEIGHBORHOOD SHOPPING CENTER"},
            {"26", "HOTEL/MOTEL"},
            {"27", "SERVICE STATION-GENERIC"},
            {"28", "GENERIC-MEDICAL/DENTAL OFFICE"},
            {"29", "REST HOME/CONVALESCENT HOPITAL"},
            {"30", "OFFICE CONDOMINIUMS"},
            {"31", "GARAGE PARKING LOT/USED CAR"},
            {"32", "TRAILER PARK"},
            {"33", "THEATER-GENERIC"},
            {"34", "BOWLING ALLEY"},
            {"35", "GENERIC-RESTAURANT/NIGHT CLUB/TAVERN"},
            {"36", "CAR WASH"},
            {"37", "GROCERY/DRUG LARGE CHAIN GENERIC"},
            {"38", "AUTO SALES/SERVICE AGENCY"},
            {"39", "GENERIC-RADIO STATION /BANK/MISC"},
            {"40", "VACANT INDUSTRIAL"},
            {"41", "FACTORY/LIGHT MANUFACTURING"},
            {"42", "FACTORY/HEAVY MANUFACTURING"},
            {"43", "WAREHOUSE-PROCESSING/STORAGE/DISTRIBUTION"},
            {"44", "STORAGE BULK CHEMICAL/OIL REFINERY"},
            {"45", "NATURAL RESOURCES – MINING, EXTRACTIVE, PROCESSING CEMENT/SILICA PRODUCTS, ROCK & GRAVEL"},
            {"46", "AUTOMOTIVE REPAIR GARAGES"},
            {"47", "INDUSTRIAL CONDOS"},
            {"49", "MISC INDUSTRIAL/SPECIAL LAND"},
            {"50", "IRRIGATED FARM VACANT WATER AVAILABLE"},
            {"51", "CITRUS"},
            {"52", "AVOCADO"},
            {"53", "VINES"},
            {"54", "TREES MISC (OTHER THAN CITRUS OR AVOCADO)"},
            {"55", "LIVESTOCK"},
            {"56", "POULTRY"},
            {"57", "IRRIGATED CROPS OTHER VEGETABLE, FLORAL, FEEDING (HAY OR SEED CROPS)"},
            {"58", "GROWING HOUSES"},
            {"59", "MISC. AGRICULTURAL"},
            {"61", "RURAL LAND OTHER"},
            {"62", "1 – 10 ACRES NON-IRRIGATED"},
            {"63", "41 – 160 ACRES NON-IRRIGATED"},
            {"64", "161 – 360 ACRES NON-IRRIGATED"},
            {"65", "361 ACRES & UP NON-IRRIGATED"},
            {"70", "INSTITUTIONAL-VACANT"},
            {"71", "CHURCH"},
            {"72", "CHURCH RECTORY, PARKING & OTHER CHURCH RELATED USE"},
            {"73", "CEMETARY"},
            {"74", "MAUSOLEUM"},
            {"75", "MORTUARY"},
            {"76", "PUBLIC BUILDING (SCHOOL, FIREHOUSE, LIBRARY, ETC)"},
            {"77", "HOSPITAL"},
            {"78", "PRIVATE SCHOOLS & FACILITIES"},
            {"79", "MISC. INSTITUTIONAL-GENERIC"},
            {"80", "VACANT RECREATIONAL"},
            {"81", "MEETING HALL, GYM - GENERIC"},
            {"82", "GOLF COURSE"},
            {"83", "MARINA DOCKS"},
            {"84", "RECREATIONAL CAMPS"},
            {"85", "NON-TAXABLE"},
            {"86", "OPEN SPACE"},
            {"87", "AGRICULTURAL PRESERVE (NOT UNDER CONTRACT)"},
            {"88", "AGRICULTURAL PRESERVE (UNDER CONTRACT)"},
            {"89", "MISCELLANEOUS/SPECIAL"},
            {"90", "VACANT TAXABLE GOVT. OWNED PROPERY"},
            {"91", "IMPROVED TAXABLE GOVT OWNED PROPERTY"}
        };

        public static Dictionary<string, string> ParcelSubTypeCodes = new Dictionary<string, string> {
            {"1", "Regular parcel with APN number"},
            {"2", "Unparcelled Private Road"},
            {"3", "Unparcelled Government Land"},
            {"4", "Unparcelled Common Area"},
            {"5", "Right-of-Way"},
            {"6", "PendParcel"},
            {"7", "Tideland Parent Parcel"},
            {"8", "Parceled Right-of-Way"}
        };

        public static Dictionary<string, string> FramingTypeMapping = new Dictionary<string, string> {
            {"A", "steel"},
            {"B", "box"},
            {"C", "masonry"},
            {"D", "wood frame"},
            {"M", "manufactured"},
            {"S", "special"},
          
        };




        public static string MapHeader(this string s)
        {
            return ColumnHeaderMapping.ContainsKey(s) ? ColumnHeaderMapping[s] : s;
        }

        public static string CleanHeader(this string s)
        {
            var arr = new string[] { ".", ",", "-", "(", ")", "'" };
            foreach (var a in arr)
            {
                s = s.Replace(a, " ");
            }

            var split = s.ToLower().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var sb = new StringBuilder();
            foreach (var sp in split)
            {
                sb.Append(sp.ToTitleCase());
            }

            return sb.ToString();
        }
        public static string ToTitleCase(this string name)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(name.ToLower());
        }

        private static Dictionary<string, NucleusUse> GetNucleusUseCodeMapping()
        {
            var dict = new Dictionary<string, NucleusUse>();
            var propertyTypesList = JsonConvert.DeserializeObject<List<NucleusUse>>(File.ReadAllText("NucleusUseCodeMapping.json"));
            foreach (var pt in propertyTypesList)
            {
                if (!dict.ContainsKey(pt.Code))
                    dict.Add(pt.Code, pt);
                else
                {
                    
                }

            }

            return dict;
            //return propertyTypesList.ToDictionary(x => x.Code);
        }

        public static IConfigurationRoot ReadWebIndexerConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("webIndexSettings.json", true, true)
                .Build();
        }
    }
}
