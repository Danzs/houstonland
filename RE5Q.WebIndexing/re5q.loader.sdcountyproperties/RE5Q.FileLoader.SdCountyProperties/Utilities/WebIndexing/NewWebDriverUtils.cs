﻿using System;
using OpenQA.Selenium;

namespace RE5Q.FileLoader.SdCountyProperties.Utilities.WebIndexing
{
    /// <summary>
    /// If an utility is missing from RE5Q.WebIndexing.Common.WebDriverUtils, ADD here while developing.
    /// Move to the RE5Q.WebIndexing.Common library prior to release and update the nuget package for that library
    /// </summary>
    public static class NewWebDriverUtils
    {
        public static bool IsElementPresent(this IWebDriver driver, By by)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            finally
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            }
        }
    }
}
